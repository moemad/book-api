const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const userSchema = new mongoose.Schema({
    userId:{
        type:Number
    },
    email:{
        type:String,
        required:true,
        unique:true,
        validate(value){
            if(!validator.isEmail(value)) throw new Error('invalid email')
        }
},
    password:{
        type:String,
        required:true,
        trim:true,
    //    match:/^([a-zA-Z0-9@*#]{6,20})$/
},
    confirmpassword:{
        type:String,
        required:true,
        trim:true,
//        match:/^([a-zA-Z0-9@*#]{6,20})$/
        // validate(confPassword){
        // if(password !== confPassword){ 
        //     throw new Error('Please Re-enter password')
        // }}
},
    username:{
        type:String,
        unique:true,
        trim:true,
        match:/^[A-Za-z]+$/
},
    fname:{
        type:String, 
        trim:true,
        match:/^[A-Za-z]+$/
},
    lname:{
        type:String, 
        trim:true,
        match:/^[A-Za-z]+$/
},
    addresses:[{
    address: {
        addrType:{type:String}, 
        addrDetails:{type:String}
    }
}],
    accountDate:{
    type:Date
},
    gender:{
        type:String,
        trim:true,
        enum:['male', 'female']
    },
    userProfile:{
    type:String
},
    accountStatus:{
    type:Boolean, 
    default:true
},
    tokens:[
    {token:{type:String, required:true}}
]
},{
    timestamps:true
})
userSchema.methods.toJSON = function(){
    const user = this.toObject()
    deleted = ['email', 'password',]
    deleted.forEach(element => {
        delete user[element]
    });
    return user
}
userSchema.pre('save', async function(next){
    lastUser = await User.findOne({}).sort({_id:-1})
    user = this
    if(!user.username)  user.username =user._id
    
    if(!lastUser) user.userId=1
    else user.userId = lastUser.userId+1    
    
    if(user.isModified('password')){
        if(user.password!=user.confirmpassword) throw new Error()
        else  user.password = await bcrypt.hash(user.password, 12)
    }
    next()
})
userSchema.methods.generateAuthToken = async function(){
    const user = this
    const token = jwt.sign({_id: user._id.toString()}, process.env.JWTKEY)
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}
userSchema.statics.loginUser = async(email, password,)=>{
    const user = await User.findOne({email})
    if(!user) throw new Error('invalid email')

    if(!user.accountStatus) throw new Error('please activate your account')

    const isvalid = await bcrypt.compare(password, user.password)
    
    if(!isvalid) throw new Error('invalid password')

    return user
}

const User = mongoose.model('User',userSchema)
module.exports=User